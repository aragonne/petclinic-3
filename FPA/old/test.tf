provider "azurerm" {
  tenant_id       = "57c345bb-f270-4952-b685-3600a4e67234"
  subscription_id = "b59e6ae6-572e-492e-81d3-0c219aac77b5"
  client_id       = "56dc12ca-f6c4-497b-b892-eae2b717fe86"
  client_secret   = "jBx7Q~etKqTDkIAZvZEL_OGIvdV-E_aY-B-a4"
  features {}
}

resource "azurerm_resource_group" "petclinic" {
  name     = "test"
  location = "West Europe"
}
resource "azurerm_virtual_network" "petclinic" {
  name                = "test"
  location            = azurerm_resource_group.petclinic.location
  resource_group_name = azurerm_resource_group.petclinic.name
  address_space       = ["10.0.0.0/16"]
}
resource "azurerm_subnet" "Prod" {
  name                 = "GatewaySubnet"
  resource_group_name  = azurerm_resource_group.petclinic.name
  virtual_network_name = azurerm_virtual_network.petclinic.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_virtual_network_gateway" "petclinic" {
  name                = "test"
  location            = azurerm_resource_group.petclinic.location
  resource_group_name = azurerm_resource_group.petclinic.name
  type     = "Vpn"
  vpn_type = "RouteBased"
  active_active = false
  enable_bgp    = false
  sku           = "Basic"
  ip_configuration {
    name                          = "vnetGatewayConfig"
    public_ip_address_id          = azurerm_public_ip.petclinic_ip.id
    private_ip_address_allocation = "Dynamic"
    subnet_id                     = azurerm_subnet.Prod.id
   
  }


  vpn_client_configuration {
    address_space = ["10.2.0.0/24"]
    root_certificate {
      name = "DigiCert-Federated-ID-Root-CA"
      public_cert_data = <<EOF
MIIDuzCCAqOgAwIBAgIQCHTZWCM+IlfFIRXIvyKSrjANBgkqhkiG9w0BAQsFADBn
MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3
d3cuZGlnaWNlcnQuY29tMSYwJAYDVQQDEx1EaWdpQ2VydCBGZWRlcmF0ZWQgSUQg
Um9vdCBDQTAeFw0xMzAxMTUxMjAwMDBaFw0zMzAxMTUxMjAwMDBaMGcxCzAJBgNV
BAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdp
Y2VydC5jb20xJjAkBgNVBAMTHURpZ2lDZXJ0IEZlZGVyYXRlZCBJRCBSb290IENB
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvAEB4pcCqnNNOWE6Ur5j
QPUH+1y1F9KdHTRSza6k5iDlXq1kGS1qAkuKtw9JsiNRrjltmFnzMZRBbX8Tlfl8
zAhBmb6dDduDGED01kBsTkgywYPxXVTKec0WxYEEF0oMn4wSYNl0lt2eJAKHXjNf
GTwiibdP8CUR2ghSM2sUTI8Nt1Omfc4SMHhGhYD64uJMbX98THQ/4LMGuYegou+d
GTiahfHtjn7AboSEknwAMJHCh5RlYZZ6B1O4QbKJ+34Q0eKgnI3X6Vc9u0zf6DH8
Dk+4zQDYRRTqTnVO3VT8jzqDlCRuNtq6YvryOWN74/dq8LQhUnXHvFyrsdMaE1X2
DwIDAQABo2MwYTAPBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIBhjAdBgNV
HQ4EFgQUGRdkFnbGt1EWjKwbUne+5OaZvRYwHwYDVR0jBBgwFoAUGRdkFnbGt1EW
jKwbUne+5OaZvRYwDQYJKoZIhvcNAQELBQADggEBAHcqsHkrjpESqfuVTRiptJfP
9JbdtWqRTmOf6uJi2c8YVqI6XlKXsD8C1dUUaaHKLUJzvKiazibVuBwMIT84AyqR
QELn3e0BtgEymEygMU569b01ZPxoFSnNXc7qDZBDef8WfqAV/sxkTi8L9BkmFYfL
uGLOhRJOFprPdoDIUBB+tmCl3oDcBy3vnUeOEioz8zAkprcb3GHwHAK+vHmmfgcn
WsfMLH4JCLa/tRYL+Rw/N3ybCkDp00s0WUZ+AoDywSl0Q/ZEnNY0MsFiw6LyIdbq
M/s/1JRtO3bDSzD9TazRVzn2oBqzSa8VgIo5C1nOnoAKJTlsClJKvIhnRlaLQqk=
EOF
    }
    revoked_certificate {
      name       = "Verizon-Global-Root-CA"
      thumbprint = "912198EEF23DCAC40939312FEE97DD560BAE49B1"
    }
  }

}

resource "azurerm_public_ip" "petclinic_ip" {
  name = "acceptanceTestPublicIp1"
resource_group_name = azurerm_resource_group.petclinic.name
location            = azurerm_resource_group.petclinic.location
  allocation_method   = "Dynamic"
  }