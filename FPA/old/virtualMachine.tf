resource "azurerm_subnet" "petclinic" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.petclinic.name
  virtual_network_name = azurerm_virtual_network.petclinic.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_interface" "petclinic" {
  name                = "petclinic"
  location            = azurerm_resource_group.petclinic.location
  resource_group_name = azurerm_resource_group.petclinic.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.petclinic.id
    private_ip_address_allocation = "Dynamic"
  }
}

# Create storage account for boot diagnostics
resource "azurerm_storage_account" "storage" {
  name                     = "diagarpjfm"
  resource_group_name      = azurerm_resource_group.petclinic.name
  location                 = azurerm_resource_group.petclinic.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "production"
  }
}
# Create (and display) an SSH key
resource "tls_private_key" "example_ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# Create virtual machine
resource "azurerm_linux_virtual_machine" "linuxvm" {
  name                  = "petclinic_vm"
  location              = azurerm_resource_group.petclinic.location
  resource_group_name   = azurerm_resource_group.petclinic.name
  network_interface_ids = [azurerm_network_interface.petclinic.id]
  size                  = "Standard_DS1_v2"

  os_disk {
    name                 = "myOsDisk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  computer_name                   = "myvm"
  admin_username                  = "azureuser"
  disable_password_authentication = true

  admin_ssh_key {
    username   = "azureuser"
    public_key = tls_private_key.example_ssh.public_key_openssh
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.storage.primary_blob_endpoint
  }
}

resource "azurerm_network_security_group" "ubuntu" {
  name                = "ubuntu-security-group1"
  location            = azurerm_resource_group.petclinic.location
  resource_group_name = azurerm_resource_group.petclinic.name

  security_rule {
    name                       = "ssh"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
resource "azurerm_network_interface_security_group_association" "ubuntu" {
    network_interface_id      = azurerm_network_interface.petclinic.id
    network_security_group_id = azurerm_network_security_group.ubuntu.id
}

output "tls_private_key" { 
  value = tls_private_key.example_ssh.private_key_pem 
  sensitive = true
}
output "admin_username" {
  value = azurerm_linux_virtual_machine.linuxvm.admin_username
}

output "vm_ip" {
  value = azurerm_public_ip.petclinic_ip.ip_address
}